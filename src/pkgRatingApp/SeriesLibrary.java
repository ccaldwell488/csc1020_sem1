package pkgRatingApp;

import java.util.ArrayList;

/**
 * @author Chris Caldwell 40188803 A Series Library class will set up an array
 *         or array list of TV Series objects and carry out appropriate
 *         functions such as adding a TV Series, editing the details of a TV
 *         Series and rating a TV Series
 */
public class SeriesLibrary {

	private ArrayList<TVSeries> library;

	public SeriesLibrary() {
		library = new ArrayList<TVSeries>();
	}

	public void addSeries(TVSeries series) {
		library.add(series);
	}

	public void editSeries() {
		// call listItems()
		//
	}

	public void rateSeries() {

	}

	public void listItems() {
		// return indexed list of tv shows, 1 version with title, 1 with full
		// details
	}

}
