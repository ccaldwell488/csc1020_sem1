package pkgRatingApp;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chris Caldwell 40188803
 *
 */
public class TVSeries {

	// Title, Genre, Rating, List of Episodes, List of Actors, List of Reviews
	private String title, genre;
	private double rating;
	private List<String> episodes, actors, reviews;

	// Constructor initialises with empty values.
	// Title and rating will be required and
	// further data from user is optional
	public TVSeries() {
		title = "";
		rating = 0.0;
		genre = "";
		episodes = new ArrayList<String>();
		actors = new ArrayList<String>();
		reviews = new ArrayList<String>();
	}

	// Accessor and mutator methods for all instance variables and a method to
	// print out the details of the TV Series

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public List<String> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<String> episodes) {
		this.episodes = episodes;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public List<String> getReviews() {
		return reviews;
	}

	public void setReviews(List<String> reviews) {
		this.reviews = reviews;
	}

	@Override
	public String toString() {

		StringBuilder details = new StringBuilder();
		// details.append("------");
		details.append(title);
		details.append("\n");
		details.append(rating);
		details.append("\n");
		details.append(genre);
		details.append("\n");
		details.append(String.join("\n", episodes));

		return details.toString();
	}

}
