package pkgRatingApp;

/**
 * Creates a dynamic menu system
 *
 * @author Chris Caldwell 40188803
 */
public class MenuObject {

	/// fields
	private final String title;
	private final String[] menuItems;

	// ----------------------------------------------------------------------------------------
	// Procedure : MenuObject
	// Date : 18/12/14
	// Author : Chris Caldwell
	// Parameters : String newTitle, String[] newMenuItems
	// Description: Allows the user to set the title and menu items
	// ----------------------------------------------------------------------------------------
	/**
	 * Creates menu object
	 *
	 * @param newTitle
	 * @param newMenuItems
	 */
	public MenuObject(String newTitle, String[] newMenuItems) {
		title = newTitle;
		menuItems = newMenuItems;
	}// menu constructor

	// ----------------------------------------------------------------------------------------
	// Procedure : display
	// Date : 18/12/14
	// Author : Chris Caldwell
	// Description: Allows the display of menu items
	// ----------------------------------------------------------------------------------------
	private void display() {
		System.out.print("\n\n\n");
		System.out.println(title);
		System.out.println("---------------");

		// For each menu item in the array, print it
		for (int i = 0; i < menuItems.length; i++) {
			System.out.printf("%s. %s\n", i + 1, menuItems[i]);
		}
	}// display

	// ----------------------------------------------------------------------------------------
	// Procedure : getChoice
	// Date : 18/12/14
	// Author : Chris Caldwell
	// Returns: int option
	// Description: Allows the user to select a menu item
	// ----------------------------------------------------------------------------------------
	/**
	 * Gets the users menu option choice
	 *
	 * @return int
	 */
	public int getChoice() {
		// Print the menu and receive the users input option
		display();
		return BasicValidation.getInteger();
	}// getChoice

}// menu