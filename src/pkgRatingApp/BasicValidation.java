package pkgRatingApp;

import java.util.Scanner;

public class BasicValidation {

	static Scanner input = new Scanner(System.in);

	public static int getInteger() {
		int data = 0;
		boolean isValid = false;
		do {
			isValid = true;
			try {
				System.out.print("Please enter a number:\n>");
				data = input.nextInt();
				// Buffer still contains CR
				input.nextLine();
			} catch (Exception e) {
				System.out.println("Whole Numbers Only!");
				isValid = false;
				input.next();
			} // catch
		} // do
		while (!(isValid));
		return data;
	}

	public static String getString(String phrase) {
		String data = "";
		boolean isValid = false;
		do {
			isValid = true;
			try {
				System.out.printf("Please enter %s:\n>", phrase);
				data = input.nextLine();
				// CHECK THAT STRING IS NOT EMPTY AND HAS NO NUMBERS
			} catch (Exception e) {
				System.out.println("Text Only!");
				isValid = false;
				input.next();
			} // catch
		} // do
		while (!(isValid));
		return data;
	}

	public static double getDouble() {
		double data = 0.0;
		boolean isValid = false;
		do {
			isValid = true;
			try {
				System.out.print("Please enter a number:\n>");
				data = input.nextDouble();
				// Buffer still contains CR
				input.nextLine();
			} catch (Exception e) {
				System.out.println("Decimals Only!");
				isValid = false;
				input.next();
			} // catch
		} // do
		while (!(isValid));
		return data;
	}

}
