package pkgRatingApp;

import java.util.List;

/**
 * @author Chris Caldwell 40188803
 *
 */
public class RatingApp_tester {

	static SeriesLibrary lib = new SeriesLibrary();
	static String title, rating, genre, episodes, actors, reviews;

	public static void main(String[] args) {
		// Create Library object

		// Create menu object and items
		String[] items = { "1. Add a TV Series", "2. Edit a TV Series", "3. Rate a TV Series", "4. Exit" };
		MenuObject menu = new MenuObject("Rating App", items);

		// Get menu choice from user
		int choice;
		while ((choice = menu.getChoice()) != 4) {

			switch (choice) {
			case 1:
				System.out.println("---Add a TV Series---");
				addSeries();
				break;
			case 2:
				System.out.println("---Edit a TV Series---");
				editSeries();
				break;
			case 3:
				System.out.println("---Rate a TV Series---");
				rateSeries();
				break;

			default:
				break;
			}
		}
		System.out.println("Goodbye!");
		/*
		 * remove sort IMDB search
		 * 
		 * pull popular popular shows #title_recs > div.rec_const_picker > div >
		 * div.rec_slide
		 * 
		 */
	}

	private static void addSeries() {
		TVSeries series = new TVSeries();
		boolean success = false;

		// Title is a requirement
		series.setTitle(BasicValidation.getString("Title"));

		// Rating is a requirement
		series.setRating(BasicValidation.getDouble());

		lib.addSeries(series);
		System.out.printf(
				"Successfully added %s with a rating of %d. Select edit in the menu to add further information.",
				series.getTitle(), series.getRating());
	}// addSeries

	private static void editSeries() {

		// PRINT ALL TV SHOWS
		// SELECT ONE BASED ON LIB INDEX

		String[] items = { "Title", "Rating", "Genre", "Episodes", "Actors", "Reviews", "Exit" };
		MenuObject menu = new MenuObject("Select an item to edit", items);
		TVSeries series = new TVSeries();
		boolean success = false;

		int choice = 0;
		while ((choice = menu.getChoice()) != 7) {
			switch (choice) {
			case 1:
				// Title is a requirement
				series.setTitle(BasicValidation.getString("Title"));

				break;
			case 2:
				series.setRating(BasicValidation.getDouble());
				break;

			case 3:
				series.setGenre(BasicValidation.getString("Genre"));
				break;
			case 4:
				List<String> episodes = series.getEpisodes();
				do {
					success = true;
					System.out.println("Add an episode?\n1. Yes\n2. No");

					// only continue loop if 1 is entered
					if ((BasicValidation.getInteger()) == 1) {
						success = false;
						episodes.add(BasicValidation.getString("episode Title"));
					}
				} while (!success);
				series.setEpisodes(episodes);
				break;
			case 5:
				List<String> actors = series.getActors();
				do {
					success = true;
					System.out.println("Add an actor?\n1. Yes\n2. No");

					// only continue loop if 1 is entered
					if ((BasicValidation.getInteger()) == 1) {
						success = false;
						actors.add(BasicValidation.getString("their Name"));
					}
				} while (!success);
				series.setActors(actors);
				break;
			case 6:
				List<String> reviews = series.getReviews();
				do {
					success = true;
					System.out.println("Add a Review?\n1. Yes\n2. No");

					// only continue loop if 1 is entered
					if ((BasicValidation.getInteger()) == 1) {
						success = false;
						reviews.add(BasicValidation.getString("review"));
					}
				} while (!success);
				series.setReviews(reviews);
				break;
			default:
				break;
			}
		}

		System.out.println(series);
	}// editSeries

	private static void rateSeries() {
	}// rateSeries

}